This is an Ansible script that shuts down an Ubuntu VM using the virt module.

To install ovirt on ubuntu:

sudo apt install -y python3-pip libvirt-dev

To run the script:

ansible-playbook -i inventory.ini script.yml


This is an Ansible script to configure network settings in an Ubuntu Virtual Machine.

Install JInja2:

pip install jinja2


network_interfaces.j2: This is a Jinja2 template file.. It is used to define the network configuration for the target system(s) where you want to configure network settings. The template file contains the desired network interface configuration using placeholders or variables that will be replaced with actual values during the template rendering process.

inventory.ini: This refers to the name of the inventory file in Ansible. The inventory file is used to define the hosts and groups of hosts that Ansible will manage. It provides information about the target systems, such as their IP addresses or hostnames, SSH connection details, and any additional variables or groupings.

Update the hosts parameter in the playbook to specify the target VM. You can use the IP address or hostname of the VM.

Run the playbook using the ansible-playbook command:

ansible-playbook -i inventory.ini script.yml

To run the .gitlab-ci.yml file and trigger the GitLab CI/CD pipeline,  push the code to the GitLab repository. 
